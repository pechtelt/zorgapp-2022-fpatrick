import java.time.LocalDate;
import java.util.*;
import java.util.ArrayList;


public class Administration {
   static List<Patient> patients = new ArrayList<>();
   MedicineList medicineList = new MedicineList();
   static Patient patient; //active patient


   // Constructor
   Administration() {
      Patient p = new Patient(0, "Verlener", "Zorg", LocalDate.of(2002, 1, 19), 1.80, 60);
      patients.add(p);

      p = new Patient(1, "Van Echtelt", "Patrick", LocalDate.of(2002, 4, 19), 1.90, 88);
      p.addMedicine(new Medicine(medicineList.getMedicines().get(0)));
      p.addMedicine(new Medicine(medicineList.getMedicines().get(1)));

      //TODO: Add hard-coded weight entries wait until .json
      patients.add(p);

      p = new Patient(2, "Treur", "Marije", LocalDate.of(2001, 3, 20), 1.70, 59);
      p.addMedicine((new Medicine(medicineList.getMedicines().get(0))));
      patients.add(p);

      p = new Patient(3, "Buijs", "Marielle", LocalDate.of(1972, 5, 26), 1.72, 86);
      p.addMedicine(new Medicine(medicineList.getMedicines().get(1)));
      patients.add(p);

      p = new Patient(4, "Benschop", "Freek", LocalDate.of(2001, 11, 17), 1.76, 94);
      patients.add(p);

      p = new Patient(5, "Achternaam", "Britte", LocalDate.of(1998, 6, 12), 1.81, 75);
      patients.add(p);

   }

   // Get patient by id
   public static Patient getPatientById(int patientId) {
      for (Patient p : patients) {
         if (p.getId() == patientId) {
            return p;
         }
      }
      return null;
   }

   // Scanner for inputPatientId
   public static int inputPatientId() {
      BScanner inputnewpatientid = new BScanner();
      return inputnewpatientid.scanInt();
   }
}

