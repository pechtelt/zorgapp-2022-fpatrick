import java.time.LocalDate;

public class Caretaker {

    // Select one of the different caretakers
    void selectCaretaker() {

        final int DOCTOR = 1;
        final int PHYSIOTHERAPIST = 2;
        final int PHARMACIST = 3;

        System.out.format("%d:  Doctor\n", DOCTOR);
        System.out.format("%d:  Physiotherapist\n", PHYSIOTHERAPIST);
        System.out.format("%d:  Pharmacist\n", PHARMACIST);

        System.out.println("Please select a caretaker:");
        BScanner scanner = new BScanner();
        int choice = scanner.scanInt();

        if (choice == 1) {
            Doctor doctor = new Doctor();
            doctor.menu(selectPatientById());
        }

        else if (choice == 2) {
            Physiotherapist physiotherapist = new Physiotherapist();
            physiotherapist.menu(selectPatientById());
        }

        else if (choice == 3) {
            Pharmacist pharmacist = new Pharmacist();
            pharmacist.menu(selectPatientById());
        }

        else {
            System.out.println("Not a valid choice, please select again");
            selectCaretaker();
        }

    }

    // Menu for the Caretaker(s)
    void menu(int patientId) {
        if (Administration.getPatientById(patientId) != null) {
            Administration.patient = Administration.getPatientById(patientId);
        }
        else {
            return;
        }

        final int STOP = 0;
        final int PRINT = 1;
        final int EDIT = 2;
        final int SELECT = 3;

        System.out.println("--------------------------------- "); //-33-
        Administration.patient.writeSelectedPatient();
        System.out.println("------------ Logout ------------ "); //12- | 7 | -12 = 33//
        System.out.format("%d:  Logout\n", STOP);
        System.out.println("------------ Patient ------------ "); //12- | 7 | -12 = 33//
        System.out.format("%d:  Print patient data\n", PRINT);
        System.out.format("%d:  Edit  patient data\n", EDIT);
        System.out.format("%d:  Select a patient\n", SELECT);
    }

    // Make a choice of the printed menu
     void makeMenuChoice(int choice) {
        final int STOP = 0;
        final int PRINT = 1;
        final int EDIT = 2;
        final int SELECT = 3;
        final int PRINTMEDICINES = 4;
        final int EDITMEDICINES = 5;
        final int PRINTWEIGHTMEASUREMENTS = 6;
        final int PRINTBMIMEASUREMENTS = 7;
        final int CHANGEMEASUREMENTS = 8;
        final int PRINTTREATMENTS = 9;
        final int ADDTREATMENTS = 10;
        final int REMOVETREATMENTS = 11;
        final int ADD = 12;

        ////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////

        var scanner1 = new BScanner(); //used for digits
        var scanner2 = new BScanner(); //used for strings
         
            switch (choice) {
                case STOP:
                    System.out.println("Successful logout!");
                    System.exit(0);
                    break;

                case ADD:
                    addPatient();
                    break;

                case PRINT:
                    Administration.patient.writePatientData();
                    break;

                case EDIT:
                    Administration.patient.editMenu();
                    break;

                case SELECT:
                    printPatientList();
                    System.out.println("Please select a patient by id:");
                    menu(Administration.inputPatientId());
                    break;

                case PRINTMEDICINES:
                    Administration.patient.writeMedicine();
                    break;

                case EDITMEDICINES: //TODO: Change, have to input 2 times and select with 0 number 1 from the list and with 1 number 2 from the list.
                    if (Administration.patient.medicines.isEmpty()) {
                        System.out.format("%s, don't use any medicines.\n", Administration.patient.getFirstName());
                        break;
                    } else
                        System.out.format("%s medicine list:\n", Administration.patient.getFirstName());
                    for (Medicine a : Administration.patient.medicines) {
                        System.out.format("%d: %s, dosage = %s\n", a.getMedId(), a.getMedName(), a.getDosage());
                    }

                    System.out.println("Please select"); //Select medicine
                    int medicineChoice = scanner1.scanInt() - 1; // Input medicine choice
                    System.out.format("Enter new dosage (was: %s) of medicine\n", Administration.patient.medicines.get(medicineChoice).getDosage());
                    String medicineDosage = scanner2.scanString();
                    Administration.patient.medicines.get(medicineChoice).setDosage(medicineDosage);
                    break;

                case PRINTWEIGHTMEASUREMENTS:
                    Administration.patient.measure.printWeightMeasurements();
                    break;

                case PRINTBMIMEASUREMENTS:
                    Administration.patient.measure.printBMIMeasurements();
                    break;

                case CHANGEMEASUREMENTS:
                    Administration.patient.measure.editMeasurement();
                    break;

                case PRINTTREATMENTS:
                    Administration.patient.treatment.printTreatments();
                    break;

                case ADDTREATMENTS:
                    Administration.patient.treatment.addTreatment();
                    break;

                case REMOVETREATMENTS:
                    Administration.patient.treatment.removeTreatment();
                    break;

                default:
                    System.out.println("This number is not valid, please enter a valid number");
                    break;
            }
        }

    //Print list of all the patients
    public void printPatientList() {
        for (Patient a : Administration.patients) {
            if (a == Administration.patients.get(0)) {
                continue;
            }
            a.writeListPatient();
        }
    }

    // Add patient
    public void addPatient() { //TODO: Add correct text
        var scanner1 = new BScanner();
        var scanner2 = new BScanner(); // use for strings
        Patient p = new Patient(1, "null", "llun", LocalDate.of(2021, 12, 11), 120, 80);

        // Select next availibe id, create function: https://stackoverflow.com/questions/56361932/java-beginner-question-method-for-finding-next-available-id-in-an-arraylist

        System.out.format("PatientID automatically set to: %d\n", getNextID());
        p.setId(getNextID());

        System.out.println("Please enter the patients firstname:");
        p.setFirstName(scanner2.scanString());

        System.out.println("Please enter the patients surname:");
        p.setSurName(scanner2.scanString());

        System.out.println("Please enter the patients date of birth (yyyy-MM-dd):");
        p.setDateOfBirth(scanner1.scanDate());

        System.out.println("Please enter the patients length (in m): ");
        p.addLength(scanner2.scanDouble()); //TODO: change setWeight

        System.out.println("Please enter the patients weight (in kg)");
        p.addWeight(scanner2.scanDouble()); //TODO: change setWeight
        Administration.patients.add(p);
        System.out.format("%s, was successfully created with id: %d ", p.getFirstName(), p.getId());

        Administration.patient = Administration.getPatientById(p.getId());
    }

    // Select next id from patient list
    public int getNextID() {
        for (int i = 0; i < Administration.patients.size(); i++) {
            if (i < Administration.patients.get(i).id) {
                return i;
            }
        }
        return Administration.patients.size();
    }

    //Select a patient ith their ID
    public static int selectPatientById() {
        Caretaker caretaker = new Caretaker();
        caretaker.printPatientList();
        System.out.format("Please select a patient bij their id \n"); //add , press 0 to add a filter: after filter function works
        int patientId = Administration.inputPatientId();
        if (patientId < 0) {
            System.out.println("This is not a valid patientID!");
            selectPatientById();
        }
        return patientId;
    }
}
