import java.time.LocalDate;
import java.util.TreeMap;

public class Measurements {

    final int GETBACK = 0;
    final int ADDMEASUREMENT = 1;
    final int REMOVEMEASUREMENT = 2;
    final int PRINTWEIGHTMEASUREMENT = 3;
    final int PRINTBMIMEASUREMENT = 4;

    private double bmi;

    // Constructor
    Measurements() {
    }

    TreeMap<LocalDate, Double> weightmeasurements = new TreeMap();
    TreeMap<Double, Double> bmimeasurements = new TreeMap<>();

    // Edit existing measurement menu
    public void editMeasurement() {
        boolean nextCycle = true;
        while (nextCycle) {
            System.out.format("%d:  Go back\n", GETBACK);
            System.out.format("%d:  Add a measurement\n", ADDMEASUREMENT);
            System.out.format("%d:  Remove a measurement\n", REMOVEMEASUREMENT);
            System.out.format("%d:  Print the weight measurements\n", PRINTWEIGHTMEASUREMENT);
            System.out.format("%d:  Print the BMI measurements\n", PRINTBMIMEASUREMENT);

            System.out.println("Please make a choice, press 0 to go back:");
            var scanner = new BScanner();
            int choice = scanner.scanInt();
            switch (choice) {
                case GETBACK:
                    nextCycle = false;
                    break;

                case ADDMEASUREMENT: //Checkpoint add (KG, date)
                    addMeasurement();
                    break;

                case REMOVEMEASUREMENT: //Remove checkpoint --> print checkpoint list, remove bij id.
                    removeMeasurement();
                    break;

                case PRINTWEIGHTMEASUREMENT:
                    printWeightMeasurementsMenu();
                    break;

                case PRINTBMIMEASUREMENT:
                    printBMIMeasurementsMenu();
                    break;

            }
        }
    }

    // Add new measurement
    public void addMeasurement() {
        var scanner = new BScanner();

        System.out.println("Please enter the measurement date (yyyy-MM-dd):");
        LocalDate date = scanner.scanDate();
        System.out.println("Please enter the weight on that date:");
        double weight = scanner.scanDouble();
        System.out.println("Please enter the length on that date:");
        double length = scanner.scanDouble();

        bmi = weight / (length / 100 * length / 100);
        weightmeasurements.put(date, weight);
        bmimeasurements.put(weight, bmi);

    }

    // Print existing measurement
    public void printMeasurements() {
        if (weightmeasurements.isEmpty()) {
            System.out.println("This patient don't have measurements yet."); //TODO: Add patient name to this function
            editMeasurement();
        } else {
            for (var p : weightmeasurements.entrySet()) {
                int kg = (int) Math.round(p.getValue());
                System.out.format("Measurement on date: [%s], with weight: %.1f (BMI: %.1f kg)\n", p.getKey().toString(), p.getValue(), bmimeasurements.get(bmi));
            }
        }
    }

    // Print Weight Measurements in overview
    public void printWeightMeasurements() {
        if (weightmeasurements.isEmpty()) {
            System.out.println("This patient don't have measurements yet.");
        } else {
            for (var p : weightmeasurements.entrySet()) {
                int kg = (int) Math.round(p.getValue());
                System.out.format("[%s] %s (%.1f kg)\n", p.getKey().toString(), "*".repeat(kg), p.getValue());
            }
        }
    }

    // Print Weight Measurements inside measurement menu, when finished they get back in measurement menu
    public void printWeightMeasurementsMenu() {
        if (weightmeasurements.isEmpty()) {
            System.out.println("This patient don't have measurements yet.");
            editMeasurement();
        } else {
            for (var p : weightmeasurements.entrySet()) {
                int kg = (int) Math.round(p.getValue());
                System.out.format("[%s] %s (%.1f kg)\n", p.getKey().toString(), "*".repeat(kg), p.getValue());
            }
        }
    }

    // Print BMI Measurements in overview
    public void printBMIMeasurements() {
        if (bmimeasurements.isEmpty()) {
            System.out.println("This patient don't have measurements yet."); //TODO: Add patient name to this function
        } else {
            for (var p : bmimeasurements.entrySet()) {
                int bmi = (int) Math.round(p.getKey());
                System.out.format("Weight: %.1f kg | %s | BMI: %.1f |\n", p.getKey(), "*".repeat(bmi), p.getValue());
            }
        }
    }

    // Print BMI Measurements inside measurement menu, when finished they get back in measurement menu
    public void printBMIMeasurementsMenu() {
        if (bmimeasurements.isEmpty()) {
            System.out.println("This patient don't have measurements yet.");
            editMeasurement();
        } else {
            for (var p : bmimeasurements.entrySet()) {
                int bmi = (int) Math.round(p.getKey());
                System.out.format("Weight: %.1f kg | %s | BMI: %.1f |\n", p.getKey(), "*".repeat(bmi), p.getValue());
            }
        }
    }

    // Remove measurement if exist
    public void removeMeasurement() {
        if (bmimeasurements.isEmpty()){
            System.out.println("This patient don't have measurements!");
         }
        else {
        printMeasurements();
        System.out.println("You can delete a measurement by typing in the measurement date (yyyy-MM-dd):");
        var choice = new BScanner();
        weightmeasurements.remove(choice.scanDate());
        }
    }
}
