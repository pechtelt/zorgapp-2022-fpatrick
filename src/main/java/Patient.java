import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;


public class Patient {
   private final int RETURN      = 0;
   private final int SURNAME     = 1;
   private final int FIRSTNAME   = 2;
   private final int NICKNAME    = 3;
   private final int DATEOFBIRTH = 4;
   private final int LENGTH      = 5;
   private final int WEIGHT      = 6;

   private String surName;
   private String firstName;
   private String nickName;
   private LocalDate dateOfBirth;

   private double weight;
   double length;
   public int    id;

   ArrayList<Medicine> medicines = new ArrayList<>();
   Measurements measure = new Measurements();
   Treatments treatment = new Treatments();

   // Constructor
   Patient( int id, String surName, String firstName, LocalDate dateOfBirth, double length, double weight)
   {
      this.id          = id;
      this.surName     = surName;
      this.firstName   = firstName;
      this.nickName    = firstName; // set via separate method if needed.
      this.dateOfBirth = dateOfBirth;
      this.length = length;
      this.weight = weight;
   }

   // Add medicine to patient
   public void addMedicine(Medicine medicine) {
      medicines.add(medicine);
   }

   public double getLength() { return length; }
   public void setLength(){
      BScanner setLength = new BScanner();
      double length = setLength.scanDouble();

      if (length < 0){
         System.out.println("The number you have entered is invalid. Number was negative. Please enter a number higher then 0.0");
         this.setLength();
      }

      else if (length == 0){
         System.out.println("The number you have entered is 0.0. 0.0m is an invalid length. Please enter a number higher then 0.0");
         this.setLength();
      }
      this.length = length;
   }

   public double getWeight() { return weight; }
   public void setWeight(){

      BScanner setWeight = new BScanner();
      double w = setWeight.scanDouble();

      if (w < 0){
      System.out.println("The number you have entered is invalid. Number was negative. Please enter a number higher then 0.0");
      this.setWeight();
      }

      else if (w == 0) {
      System.out.println("The number you have entered is 0.0. 0.0kg is weightless. Please enter a number higher then 0.0");
      this.setWeight();
      }
      weight = w;
   }

   private void setDateOfBirth() {
      BScanner setBirthDate = new BScanner();
      dateOfBirth = setBirthDate.scanDate();
      }
   public void setDateOfBirth(LocalDate dateOfBirth) {
      this.dateOfBirth = dateOfBirth;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }
   public String getFirstName() { return firstName; } // Access surName

   public void setSurName(String surName) {
      this.surName = surName;
   }
   public String getSurName() { return surName; }     // Access surName

   public void setId(int id) {
      this.id = id;
   }
   public int getId() { return id; }

   public void setNickName() {
      BScanner setNickName = new BScanner();
      nickName = setNickName.scanString();
   }
   public String getNickName() { return nickName; }   // Access callName

   public double calcBMI() {
      return weight / (length * length);
   }
   public int calcAGE() {
      return Period.between(dateOfBirth, LocalDate.from(LocalDateTime.now())).getYears();
   }

   public void addWeight(Double weight) {
      this.weight = weight;
   }
   public void addLength(Double length) {
      this.weight = length;
   }

   // Menu for the Patients
   static void menu(int patientId)
   {
      if (Administration.getPatientById(patientId) != null) {
         Administration.patient = Administration.getPatientById(patientId);
      }

      else {return;}

      final int STOP       = 0;
      final int PRINT      = 1;
      final int EDIT       = 2;
      final int PRINTMED   = 3;

      var scanner = new BScanner();

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.format( "%s\n", "=".repeat( 80 ) );
         System.out.format( "Current login: " );
         Administration.patient.writeOneliner();

         ////////////////////////
         // Print menu on screen
         ////////////////////////
         System.out.format( "%d:  Logout\n", STOP );
         System.out.format( "%d:  Print patient data\n", PRINT );
         System.out.format( "%d:  Edit  patient data\n", EDIT );
         System.out.format( "%d:  Print medicine data\n", PRINTMED );
         ////////////////////////

         System.out.println( "enter digit:" );
         int choice = scanner.scanInt();
         switch (choice) {
            case STOP: // interrupt the loop
               System.out.println("Successful logout!");
               System.exit(0);
               break;
            case PRINT:
               Administration.patient.writePatientData();
               break;

            case EDIT:
               Administration.patient.editPatient();
               break;

            case PRINTMED:
               Administration.patient.writeMedicine();
               break;

            default:
               System.out.println("Please enter a valid number");
               break;
         }
      }
   }

   // Handle editing of patient data
   public void editMenu()
   {
      var scanner1 = new BScanner(); // use for numbers.
      var scanner2 = new BScanner(); // use for strings

      boolean nextCycle = true;
      while (nextCycle)
      {
         System.out.println( "Patient data edit menu:" );
         editMenuCaretaker();
         System.out.println( "Enter digit (0=return)" );

         int choice = scanner1.scanInt();
         switch (choice) {
            case RETURN -> nextCycle = false;
            case SURNAME -> {
               System.out.format("Enter new surname: (was: %s)\n", getSurName());
               surName = scanner2.scanString();
               System.out.format("Surname changed to: %s \n", surName);
            }
            case FIRSTNAME -> {
               System.out.format("Enter new first name: (was: %s)\n", getFirstName());
               firstName = scanner2.scanString();
               System.out.format("Firstname changed to: %s \n", firstName);
            }
            case NICKNAME -> {
               System.out.format("Enter new nickname: (was: %s)\n", getNickName());
               nickName = scanner2.scanString();
               System.out.format("Nickname changed to: %s \n", nickName);
            }
            case DATEOFBIRTH -> {
               System.out.format("Enter new date of birth (yyyy-MM-dd; was: %s)\n", dateOfBirth);
               setDateOfBirth();
               System.out.format("Date of birth changed to: %s \n", dateOfBirth);
            }
            case LENGTH -> {
               System.out.format("Enter new length (in m; was: %.2f)", getLength());
               setLength();
               System.out.format("Length changed to: %s \n", length);
            }
            case WEIGHT -> {
               System.out.format("Enter new weight (in kg; was: %.1f)\n", getWeight());
               setWeight();
               System.out.format("Weight changed to: %s \n", weight);
            }
            default -> System.out.println("Invalid entry: " + choice);
         }
      }
   }

   // Patient can only edit his nickname
   void editPatient()
   {
      System.out.println("You can only change you're Nickname as a Patient.");
      System.out.format("Please enter a new Nickname: (was: %s)\n", nickName );
      setNickName();
      System.out.format( "Nickname change to: %s\n", nickName );

   }

   // Menu of caretaker to edit patient data.
   void editMenuCaretaker()
   {
      System.out.format( "%d: %-17s %s\n", SURNAME, "Surname:", surName );
      System.out.format( "%d: %-17s %s\n", FIRSTNAME, "firstName:", firstName );
      System.out.format( "%d: %-17s %s\n", NICKNAME, "Nickname:", nickName );
      System.out.format( "%d: %-17s %s (age %d)\n", DATEOFBIRTH, "Date of birth:", dateOfBirth, calcAGE() );
      System.out.format( "%d: %-17s %.2f\n", LENGTH, "Length:", length );
      System.out.format( "%d: %-17s %.2f (bmi=%.1f)\n", WEIGHT, "Weight:", getWeight(), calcBMI() );
   }

   // Write selected patient data
   void writePatientData()
   {
      System.out.println( "===================================" );
      System.out.format( "%-17s %s\n", "Surname:", surName );
      System.out.format( "%-17s %s\n", "firstName:", firstName );
      System.out.format( "%-17s %s\n", "Nickname:", nickName );
      System.out.format( "%-17s %s (age %d)\n", "Date of birth:", dateOfBirth, calcAGE() );
      System.out.format( "%-17s %.2f\n", "Length:", length );
      System.out.format( "%-17s %.2f (bmi=%.1f)\n", "Weight:", getWeight(), calcBMI() );
      System.out.println( "===================================" );
   }

   // If patient has medicines then write medicines
   public void writeMedicine(){
      if (medicines.isEmpty()){
         System.out.format("%s, you don't use medicines.\n", firstName);
      }
      else
         System.out.println("You're medicine list:");
      for ( Medicine a : medicines) {
         System.out.println( "===================================" );
         System.out.format("Medicine name: %s\n",a.getMedName());
         System.out.format("Medicine description: %s\n",a.getMedDesc());
         System.out.format("Medicine type: %s\n",a.getMedType());
         System.out.format("Medicine dosage: %s\n",a.getDosage());

      }
   }

   // Write oneline info of patient to screen.
   void writeOneliner()
   {
      System.out.format( "%10s %-20s [%s] \n", firstName, surName, dateOfBirth.toString() );
   }

   // Write line who the selected patient is
   void writeSelectedPatient()
   {
      System.out.format( "Selected Patient: %-5s %-10s %-10s [%s]\n", id, firstName, surName, dateOfBirth.toString() );
   }

   // Checks if patient has medicines or not.
   public String checkMedicineYesNo(){
      if(medicines.isEmpty()) {
         return "No";
      }
      else {
         return "Yes";
      }
   }

   // Checks if patient has measurements or not.
   public String checkMeasurementsYesNo(){
      if(measure.bmimeasurements.isEmpty()) {
         return "No";
      }
      else {
         return "Yes";
      }
   }

   // Write list of patients at the beginning
   void writeListPatient(){
      System.out.format( "%-5s %-10s %-20s [%s] | Medicines: %s | Measurements: %s\n", id, firstName, surName, dateOfBirth.toString(), checkMedicineYesNo(), checkMeasurementsYesNo() );
   }
}
