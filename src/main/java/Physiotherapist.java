public class Physiotherapist extends Caretaker {

    @Override
    void menu(int patientId) {

        final int PRINTWEIGHTMEASUREMENTS = 6;
        final int PRINTBMIMEASUREMENTS = 7;
        final int CHANGEMEASUREMENTS = 8;
        final int PRINTTREATMENTS = 9;
        final int ADDTREATMENTS = 10;
        final int REMOVETREATMENTS = 11;

        while (true) {
            super.menu(patientId);
            System.out.println( "----------- Measurement ----------- " ); //11- | 11 | -11 = 33//
            System.out.format( "%d:  Print weight measurement(s)\n", PRINTWEIGHTMEASUREMENTS );
            System.out.format( "%d:  Print BMI measurement(s)\n", PRINTBMIMEASUREMENTS );
            System.out.format( "%d:  Edit measurement(s)\n", CHANGEMEASUREMENTS );
            System.out.println( "----------- Treatments ------------ " ); //11- | 10 | -12 = 33//
            System.out.format( "%d:  Print treatment(s)\n", PRINTTREATMENTS );
            System.out.format( "%d:  Add treatment\n", ADDTREATMENTS );
            System.out.format( "%d:  Edit treatment\n", REMOVETREATMENTS );
            System.out.println( "--------------------------------- " ); //12- | 8 | -13 = 33//
            System.out.println("Please make a choice:");
            BScanner scanner = new BScanner();
            int choice = scanner.scanInt();
            if (choice == 0 ||
                choice == 1 ||
                choice == 2 ||
                choice == 3 ||
                choice == PRINTWEIGHTMEASUREMENTS ||
                choice == PRINTBMIMEASUREMENTS ||
                choice == CHANGEMEASUREMENTS ||
                choice == PRINTTREATMENTS ||
                choice == ADDTREATMENTS ||
                choice == REMOVETREATMENTS) {
                makeMenuChoice(choice);
            }
            else {
                System.out.println("Not a valid choice!");
            }
        }
    }
}

